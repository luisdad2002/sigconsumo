# Sistema de Gestión Integral de Cooperativas de Consumo
El SIGConsumo es un sistema de información basado en Odoo que permite la gestión de todo el proceso de planificación y ejecución del consumo por parte de organizaciones de base popular en su búsqueda por saltar la especulación de precios relacionado a la cadena de distribución.


## Módulos:
### Encuestas de opinión
permite realizar encuestas rápidas y sencillas para hacer más facil la toma de decisiones en base a la participación de la mayoría

### Registro de voluntades, asociados, ex-asociados (partners)
permite gestionar la información, el estatus y la participación

Módulo de Contactos de Odoo, con algunas modificaciones menores

### Gestión de proveedores y sus cotizaciones 
permite gestionar la información de todos los proveedores así como sus cotizaciones de precios.

Módulo de Compras de Odoo. Modificar para poder gestionar "Productores"

### Gestión de productos
permite gestionar todos los productos que se compran/venden/intercambian. Se debe tener una asociación de los proveedores de cada producto

### Gestión de asambleas
Permite gestionar los puntos de agenda, decisiones finales, tareas asumidas y asistentes. Una asamblea puede estar asociada a un ciclo de consumo (Ordinaria) o puede ser extraordinaria.

### Gestión de jornadas
se gestionan los productos a intercambiar y sus cantidades, asistentes, lugar, fortalezas y debilidades detectadas.

Al programar una jornada se debe enviar automáticamente un correo a todos los socios y voluntarios


### Gestión de ingresos
se puede pedir dinero adelante si nosotros cubrimos todo
se puede pagar el dia de la jornada si el productor llega hasta el lugar
se puede pagar el dia de la jornada si el productor trabaja a consignación

### Gestión de gastos
Gestiona todos los gastos que se incurran... Gastos ordinarios (de un ciclo) gastos extraordinarios.

### Gestión de Producción
¿Quien produce? ¿Qué produce? ¿Cuanto Produce? ¿Donde produce? ¿Qué recibe a cambio?

Puede ser un nuevo tipo de partner llamado "Productor"

En el momento de la asamblea hay que preguntar si hay alguien que esté produciendo algo  que desee ponerlo a la venta durante la jornada

## Proceso de adhesión y exclusión de asociados:
### .
### ..

## Proceso o Ciclo de consumo:
### planificación del consumo
### jornada de consumo
### rendición de cuentas